package pengjam.boonyarit.lab6;

import java.awt.*;

import javax.swing.*;

public class BankAccount {
	public static void addComponents(Container bAcct) {
		bAcct.setLayout(new GridLayout(3, 2));
		JLabel preBalance = new JLabel("Previous Balance:");
		JLabel amount = new JLabel("Amount:");
		JLabel curBalance = new JLabel("Current Balance:");

		JTextField preBalanceInput = new JTextField("1000", 20);
		JTextField amountInput = new JTextField(20);
		JTextField curBalanceInput = new JTextField(20);

		bAcct.add(preBalance);
		bAcct.add(preBalanceInput);
		bAcct.add(amount);
		bAcct.add(amountInput);
		bAcct.add(curBalance);
		bAcct.add(curBalanceInput);

		amountInput.setEditable(false);
		curBalanceInput.setEditable(false);

	}

	public static void addButtonPanel(Container bPane) {
		bPane.add(new JButton("Withdraw"));
		bPane.add(new JButton("Deposit"));
		bPane.add(new JButton("Exit"));
	}

	public static void main(String[] args) {
		JFrame window = new JFrame("Simple Bank Account");
		JPanel content = new JPanel();
		JPanel buttonPanel = new JPanel();

		addComponents(content);
		addButtonPanel(buttonPanel);

		window.setLayout(new BorderLayout());
		window.add(content, BorderLayout.NORTH);
		window.add(buttonPanel, BorderLayout.AFTER_LAST_LINE);

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = window.getSize().width;
		int h = window.getSize().height;
		int x = (dim.width - w) / 2 - 250;
		int y = (dim.height - h) / 2 - 100;

		window.pack();
		window.setLocation(x, y);
		window.setVisible(true);

	}
}