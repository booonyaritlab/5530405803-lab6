package pengjam.boonyarit.lab6;

import java.awt.*;

import javax.swing.*;

public class Calculator {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	private static void createAndShowGUI() {
		JFrame.setDefaultLookAndFeelDecorated(true);
		JFrame window = new JFrame("Simple Calculator");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel component = new JPanel();
		JPanel buttonPanel = new JPanel();

		addComponents(component);
		addbuttonPanel(buttonPanel);

		window.setLayout(new BorderLayout());
		window.add(component, BorderLayout.NORTH);
		window.add(buttonPanel, BorderLayout.AFTER_LAST_LINE);

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int w = window.getSize().width;
		int h = window.getSize().height;
		int x = (dim.width - w) / 2 - 250;
		int y = (dim.height - h) / 2 - 100;

		window.setLocation(x, y);
		window.pack();
		window.setVisible(true);
	}

	private static void addComponents(Container component) {
		component.setLayout(new FlowLayout());
		JTextField textDisPlay = new JTextField(35);
		component.add(textDisPlay);

	}

	private static void addbuttonPanel(Container bPanel) {
		bPanel.setLayout(new GridLayout(4, 4, 5, 5));

		String[] contentButton = { "1", "2", "3", "4", "5", "6", "7", "8", "9",
				"0", "+", "-", "*", "/", "%", "=" };

		JButton[] button = new JButton[contentButton.length];
		JPanel[] buttonPanel = new JPanel[contentButton.length];

		for (int i = 0; i < contentButton.length; i++) {
			button[i] = new JButton(contentButton[i]);
			buttonPanel[i] = new JPanel();
			buttonPanel[i].add(button[i]);
			if (i < 10)
				buttonPanel[i].setBackground(Color.black);
			else
				buttonPanel[i].setBackground(Color.red);
			bPanel.add(buttonPanel[i]);
		}
	}

}
